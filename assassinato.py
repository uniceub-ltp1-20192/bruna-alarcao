import sys

man = ["renato faca", "roberto arma", "uesllei"]
woman = ["maria calice", "roberta corda"]
whiteWeapons = ["faca", "corda", "calice"]

def askName():
    suspect = input("Digite o nome da pessoa: ")
    return suspect

def validateString(suspect):
    return not suspect.isdecimal()

def validateVogals(suspect):
    counter = 0
    for letter in suspect:
        if letter.lower() in "aei":
            counter += 1
    return counter >= 3  and "o" not in suspect.lower() and "u" not in suspect.lower()

def getBioGender(suspect):
    if suspect.lower() in man:
        return "Male"
    if suspect.lower() in woman:
        return "Female"

def getWeapon(suspect):
    hasWeapon = False
    for weapon in whiteWeapons:
        if weapon in suspect.lower():
            hasWeapon = True
    return hasWeapon

def validateAssassinWoman(biogender, weapon):
    return biogender == "Female" and weapon

def validateAssassinMan(biogender, time):
    return biogender == "Male" and time == "00:30"




name = askName()

# verificar se o suspect é uma string
vn = validateString(name)

if not vn:
    sys.exit("Não sabe nem o que é um nome")

# verifica se o suspect respeita a RN1 (vogais)
vv = validateVogals(name)

if not vv:
    sys.exit("Não o que vem a seguir: Suspeito! Forte! Foi você!")


biogender = getBioGender(name)
weapon = getWeapon(name)

time = ""
if biogender == "Male":
    time = input("Que horas você estava no saguão? (hh:mm)")

vw = validateAssassinWoman(biogender, weapon)
vm = validateAssassinMan(biogender, time)

if not (vw or vm):
    sys.exit("Não o que vem a seguir: Suspeito! Forte! Foi você!")

print("ASSSSAAAAASSSSSIIIIINOOOOO(AAAAA)!!!!!!!!!!!!!!!!!!!!!!!!!!")