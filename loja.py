def validarNumero(entrada):
    return entrada.isnumeric()

def validarDecimal(entrada):
    if '.' in entrada or ',' in entrada:
        entrada = entrada.replace('.','',1)
        entrada = entrada.replace(',','',1)
    validado = True
    numeros = ['0','1','2','3','4','5','6','7','8','9']
    for letra in entrada:
        if letra not in numeros:
            validado = False
    return validado


def desejaContinuar():
    continuar = True
    opcao = input("Deseja Continuar? Se sim, digite 'sim': ")
    if (opcao.lower() != 'sim'):
        continuar = False
    return continuar


def repetirQuantidade(quantidade):
    total = 0
    totalComDesconto = 0
    for i in range(quantidade):
        valor = input("Qual valor do produto " + str(i + 1) + ": ")
        desconto = input("Qual o desconto do produto " + str(i + 1) + " (0 - 100): ")
        if (not validarDecimal(valor) or not validarDecimal(valor)):
            print("Número inválido!")
            continuar = desejaContinuar()
            if (continuar):
                continue
            else:
                break

        valor = float(valor)
        desconto = float(desconto)

        total += valor
        totalComDesconto += valor * (1 - (desconto / 100))

    print("Total: ", total)
    print("Total com Desconto: ", totalComDesconto)

def programa():
    continuar = True
    while(continuar):

        quantidade = input("Qual a quantidade de produtos: ")

        if(not validarNumero(quantidade)):
            print("Número inválido!")
            continuar = desejaContinuar()
            if(continuar):
                continue
            else:
                break

        quantidade = int(quantidade)
        repetirQuantidade(quantidade)

        continuar = desejaContinuar()


programa()